FROM ruby:2.3
  
RUN apt-get update && apt-get install -y --no-install-recommends postgresql-client && apt-get install -y nodejs --no-install-recommends && ln -sf /usr/bin/nodejs /usr/local/bin/node && rm -rf /var/lib/apt/lists/*
WORKDIR /usr/src/app
COPY Gemfile* ./
RUN gem install bundler && bundle install && rm -rf /var/lib/apt/lists/*
COPY . .
CMD ["rails", "server", "-b", "0.0.0.0"]
